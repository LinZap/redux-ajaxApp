# React-Awesome

React-Awesome 是一套使用 React+Redux 開發的 Web Framework，並透過 Webpack 做為網站部屬與打包工具。

## Installation

安裝必要 packages
````
npm install
````

安裝必要 components
````
bower install
````

## Launch
````
npm run dev
````

