var webpack = require('webpack');

const bower_dir = __dirname + '/bower_components',

config = {

	addVendor: function (name, path) {
		this.resolve.alias[name] = path;
		this.entry.vendors.push(name);
		this.module.noParse.push(new RegExp('^' + name + '$'));
	},

	devtool: 'cheap-module-eval-source-map',//'eval',

    watch: true,

	entry: {
		app: [__dirname + '/app/boot.jsx'],
		vendors: []
	},

	resolve: { 
		alias: {} ,
		//extensions: ['','.jsx', '.js', '.styl']
	},

	plugins: [
		new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js'), 
		new webpack.HotModuleReplacementPlugin(),
		//new webpack.NoErrorsPlugin()
	],

	output: {
		path: __dirname + '/build',
		filename: 'bundle.js',
		//publicPath: '/build/',
	},

	module: {
		noParse: [],

		loaders: [
			{
				test: /\.jsx?$/,
				loader: 'babel',
				query: { presets: ['es2015','react', 'stage-2'] , compact: false }
			},
			// {
			// 	test: /\.js?$/,
			// 	loader: 'babel',
			// 	query: { presets: ['es2015'] , compact: false }
			// },
			{
				test: /\.styl$/,
				loader: 'style-loader!css-loader!stylus-loader'
			},
			
			{
				test: /.*\.(gif|png|jpe?g|svg)$/i,
				loaders: [
					'file?hash=sha512&digest=hex&name=[hash].[ext]',
					'image-webpack'
				]
			}
		]
		
	},

	imageWebpackLoader: {
		pngquant:{
			quality: "65-90",
			speed: 4
		},
		svgo:{
			plugins: [{
				removeViewBox: false
			},
			{
				removeEmptyAttrs: false
			}]
		}
	}
};


config.addVendor('react', bower_dir + '/react/react.js');
config.addVendor('react-dom', bower_dir + '/react/react-dom.min.js');


module.exports = config;