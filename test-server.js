var webpack = require("webpack"),
    webpackDevServer = require("webpack-dev-server"),
    opn = require("opn"),
    fs = require("fs"),
    webpackConfig = require("./webpack.config.js"),
    bundleStart = null,
    server = {
        host: 'localhost',
        port: 888
    };

webpackConfig.entry.app = [__dirname + '/test/boot.jsx'];

var compiler = webpack(webpackConfig);

webpackConfig.entry.app.unshift(
    'webpack/hot/dev-server',
    'webpack-dev-server/client?http://'+server.host+':'+server.port);

compiler.plugin('compile', function() {
    console.log('Bundling...');
    bundleStart = Date.now();
});

compiler.plugin('done', function() {
    console.log('Bundled in ' + (Date.now() - bundleStart) + 'ms!');
});


var bundler = new webpackDevServer(compiler, {
    contentBase: 'build',
    //publicPath: '/build/',
    watchOptions: {
        aggregateTimeout: 1000,
        poll: 1000
    },
    historyApiFallback: false,
    hot: true,
    quiet: false,
    noInfo: false,
    stats: { colors: true }
});

bundler.listen(server.port, server.host, function() {
    opn('http://'+server.host+':'+server.port);
});