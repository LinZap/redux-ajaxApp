import { combineReducers } from 'redux'
import { REQ_POST,REC_POST,FAI_POST } from './action.js'


function post(state={posts:[],isfetching:false,failure:false},action){

	switch(action.type) {
		case REQ_POST: 
			return {
				...state,
				isfetching: true,
				failure: false,
			}
		case REC_POST: 
			return {
				...state,
				isfetching: false,
				failure: false,
				posts: action.post,
			}
		case FAI_POST:
			return {
				...state,
				isfetching: false,
				failure: true
			}
		default: 
			return state
	}
}

const rootReducer = combineReducers({
	postState: post,
})

export default rootReducer
