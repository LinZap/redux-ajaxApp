import React, { Component } from 'react'
import img_like from './img/like.png'
import img_dislike from './img/dislike.png'

export default class Post extends Component{

	render(){
		const {data} = this.props

		return (<div className='post'>

			<div className='profile'>
				<img src={`http://graph.facebook.com/${data.mfbid}/picture`}/>
				<a className='name' href={`http://www.facebook.com/${data.mfbid}`} target='_blank'>{data.mname}</a>
				<a className='date' href={`http://www.facebook.com/${data.fbid}`} target='_blank'>{data.since}</a>
			</div>

			<div className='des'>
				{
					data.edes?
					(data.edes.length>50?data.edes.slice(0,50)+'...':data.edes)
					:null
				}
			</div>

			<a className='link' href={data.url} target='_blank'>
				<img src={data.photo}/>
				<h3>{data.urlname}</h3>

				{
					data.urldes?
					(<p>{data.urldes.length>150?data.urldes.slice(0,150)+'...':data.urldes}</p>)
					:null
				}

			</a>

			<div className='info'>
				<span className='like'>
					<img src={img_like}/>
					<span>{data.kcount}</span>
				</span>
				<span className='share'>
					<img src={img_dislike}/>
					<span>{data.scount}</span>
				</span>
			</div>

		</div>)
	}

}
