import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addItem,fetchPosts } from './action.js'
import List from './List.jsx'
import PostList from './PostList.jsx'

class TestApp extends Component{

	constructor(props) {
		super(props) 
	}
	
	componentDidMount(){
		console.log('props',this.props);
	}

	render(){
		const {data,posts,dispatch} = this.props
		return (
			<div>
				<button onClick={this.onFetchPostsHandler(dispatch)}>Fetch Posts</button>
				<PostList posts={posts} />
			</div>
		)
	}

	onFetchPostsHandler(dispatch){
		return e=>{
			dispatch(fetchPosts(10))
		}
	}
}

/* 將status轉換成prop */
function mapStateToProps(state) {

	const { itemState,postState } = state

	return { 
		posts: postState
	}
}


/*
	connect 將 status 轉換或屬到 messenege
*/
export default connect(mapStateToProps)(TestApp)