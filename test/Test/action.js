import fetch from 'isomorphic-fetch'

export const REQ_POST = 'REQ_POST'
export const REC_POST = 'REC_POST'
export const FAI_POST = 'FAI_POST'


function requestPosts(){
	return {
		type: REQ_POST
	}
}

function receivePosts(posts){
	return {
		type: REC_POST,
		post: posts
	}
}

function failurePosts(){
	return {
		type: FAI_POST
	}
}


export function fetchPosts(limit=10){
	return dispatch => {
		dispatch(requestPosts())
		return fetch(`http://163.22.21.72:888/api/jsonfeed_fba.php?cmd=myfeed&limit=${limit}`)
		.catch(e => dispatch(failurePosts()))
		.then(response => response.json())
		.then(json => dispatch(receivePosts(json.item)))
	}
}

