import React, { Component } from 'react'
import stylus from './styl/PostList.styl'
import Post from './Post.jsx'
import img_loading from './img/loading.gif'

export default class PostList extends Component{
	render(){
		const {posts,isfetching,failure} = this.props.posts

		return (
		<div className='container'>{

			failure? (<h3>can not fetch posts</h3>)
			:isfetching? (<img src={img_loading}/>)
			:posts.length? posts.map(p=><Post data={p} key={p.oid}/>)
			:(<h3>No any data</h3>)
		
		}</div>)
	}
}