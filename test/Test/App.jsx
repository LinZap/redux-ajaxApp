import React, { Component } from 'react'
import { Provider } from 'react-redux'
import TestApp from './TestApp.jsx'
import testApp from './reducers.js'
import configureStore from './configureStore.js'

const store = configureStore() 

export default class Root extends Component{

	render(){
		return(
			<Provider store={store}>
				<TestApp />
			</Provider>
		)
	}

}